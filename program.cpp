//
// Created by kroten on 14.04.2020.
//

#include "program.h"

Program::Program(const std::multimap<int, Request*> &description, SkipList* lst) {
    this->description = description;
    //auto it = description.rbegin();
    //this->lastStep = it->first;
    //this->name = it->second->get_name();
    this->lst = lst;
}

std::multimap<int, Request*>::iterator Program::deleteCell(std::multimap<int, Request*>::iterator it) {
    this->lst->deleteRequest(it->second);
    return(this->description.erase(it));
}

int Program::doRequest(int start, int step) {
    if(this->description.find(step - this->frozen_step - start) != this->description.end()) {
        auto t = this->description.equal_range(step - this->frozen_step - start);
        auto it = t.first;
        advance(it, this->count_req);
        while (it != t.second) {
            it->second->give_program(reinterpret_cast<int *>(this));
            this->status = this->lst->inseretRequest(it->second);

            this->lst->showList();
            if (this->status == NULL) {
                this->count_req++;
                if (!it->second->check_flag())
                    it = deleteCell(it);
                else
                    ++it;
            } else {
                this->frozen_step= this->frozen_step+1;
                return 1;

            }
        }

        this->count_req = 0;

    }

    if(step - this->frozen_step - start == lastStep){
        for(auto it = this->description.begin(); it != this->description.end();){
            this->lst->deleteRequest(it->second);
            ++it;
        }
        this->lst->showList();
        return 2;
    }
    return 0;
}

    NodeList* Program::get_status() {
    return this->status;
}

