#include "request.h"

Request::Request(const std::string &name, int start, Types request_type) {
    this->name = name;
    this->start = start;
    this->request_type = request_type;
}

int Request::get_start() const {
    return this->start;
}

bool Request::operator==(const Request &other) {
    return this->start==other.start;
}

bool Request::operator<(const Request &other) {
    return this->start<other.start;
}

Types Request::check_type() {
    return this->request_type;
}

std::string Request::get_name() const {
    return this->name;
}

void Request::give_program(int *program) {
    this->p_program = program;
}

int* Request::get_program() {
    return this->p_program;
}








