/*!
 * @file
 * cpp file with Cell class tests
 */
#include <cassert>
#include "cell.h"

void test_Cell(){
    // create a request for the program touch
    Cell touch("touch", 3, write, true);
    // check request flag
    assert(touch.check_flag());
    // create a request for the program cat
    Cell cat("touch", 3, read, false);
    // check request flag
    assert(!cat.check_flag());
    assert(touch.get_end() == 3);
}

int main(){
    test_Cell();
    return 0;
}