#include "skiplist.h"
#include "area.h"
#include "cell.h"
int main() {
    Area cat("cat", 2, 10, read);
    Area cp1("cp1", 11, 13, write);
    Cell cp2("cp2", 2, read, false);
    Area cat1("cat1", 14, 16, read);
    Area cat2("cat2", 14, 20, read);
    Area cat3("cat3", 12, 15, read);
    Area cat4("cat4", 16, 27, read);
    SkipList lst(5, 0.8);
    lst.inseretRequest(&cat);
    lst.inseretRequest(&cp2);
    lst.inseretRequest(&cp1);
    lst.inseretRequest(&cat1);
    lst.inseretRequest(&cat2);
    lst.inseretRequest(&cat3);
    lst.inseretRequest(&cat4);
    lst.showList();
    lst.deleteRequest(&cat);
    lst.deleteRequest(&cat4);
    lst.showList();
    return 0;
}
