#include "skiplist.h"
#include "area.h"
#include "cell.h"
#include <string.h>
#include <algorithm>
#include <iostream>

bool endLow(Request* a, Request* b) {
    return (a->get_end() < b->get_end());
}

NodeList::NodeList(int lvl, Request *request) {
    this->reqList.push_back(request);
    forward = new NodeList*[lvl+1];
    memset(forward, 0, sizeof(NodeList*)*(lvl+1));
}


SkipList::SkipList(int MaxLvl, float P) {
    this->MaxLvl = MaxLvl;
    this->P = P;
    lvl = 0;
    Cell init("init", -1, write, true);
    header = new NodeList(MaxLvl, &init);
}

int SkipList::randomLevel() {
    float r = (float)rand()/RAND_MAX;
    int lvl = 0;
    while (r < P && lvl < MaxLvl) {
        lvl++;
        r = (float)rand()/RAND_MAX;
    }
    return lvl;
}

NodeList* SkipList::inseretRequest(Request *request) {
    NodeList *current = header;
    NodeList *currentLow = header;
    NodeList *update[MaxLvl+1];
    memset(update, 0, sizeof(NodeList*)*(lvl+1));
    bool flag_insert = true;
    for (int i = lvl; i >=0; i--) {
        while (current->forward[i] != NULL && *(current->forward[i]->reqList.back()) < *(request))
            current = current->forward[i];
        update[i] = current;
    }
    current = current->forward[0];

    if (request->check_type() == write) {
        while (currentLow->forward[0] != NULL) {
            if ((request->get_start() <= currentLow->forward[0]->reqList.back()->get_end()) &&
                (request->get_end() >= currentLow->forward[0]->reqList.back()->get_start())) {
                flag_insert = false;
                break;
            }
            currentLow = currentLow->forward[0];
        }
    } else {
        while (currentLow->forward[0] != NULL) {
            if (currentLow->forward[0]->reqList.back()->check_type() == write)
                if ((request->get_start() <= currentLow->forward[0]->reqList.back()->get_end()) &&
                    (request->get_end() >= currentLow->forward[0]->reqList.back()->get_start())) {
                    flag_insert = false;
                    break;
                }
            currentLow = currentLow->forward[0];
        }
    }
    if (!flag_insert) {
        std::cerr << "Entry was found" << std::endl;
        return currentLow->forward[0];
    } else {
        if(current != NULL && *(current->reqList.back()) == *(request)) {
            auto it = std::upper_bound(current->reqList.begin(), current->reqList.end(), request, endLow);
            current->reqList.insert(it, request);
        } else {
            int rlvl = randomLevel();
            if (rlvl > lvl) {
                for(int i = lvl+1; i<rlvl+1; i++)
                    update[i] = header;
                lvl = rlvl;
            }

            NodeList *n = new NodeList(rlvl, request);

            for (int i = 0; i<rlvl+1;i++) {
                n->forward[i] = update[i]->forward[i];
                update[i]->forward[i] = n;
            }
            return NULL;
        }

    }


}

void SkipList::showList() {
    std::cout<<"\n---------------------------- Disk ----------------------------"<<"\n";
    for (int i=0;i<=lvl;i++)
    {
        NodeList *node = header->forward[i];
        std::cout << "Level " << i << ": ";
        while (node != NULL)
        {
            std::cout << "[" << node->reqList.back()->get_start()<<" "<< node->reqList.back()->get_end()<<"] ";
            node = node->forward[i];
        }
        std::cout << "\n";
    }
}

void SkipList::deleteRequest(Request *request) {
    NodeList* current = header;
    NodeList *update[MaxLvl+1];
    memset(update, 0, sizeof(NodeList*)*(MaxLvl+1));

    for(int i = lvl; i >= 0; i--)
    {
        while(current->forward[i] != NULL  && *(current->forward[i]->reqList.back()) < *(request))
            current = current->forward[i];
        update[i] = current;
    }

    current = current->forward[0];

    if(current != NULL && *(current->reqList.back()) == *(request)) {
        current->reqList.erase(std::remove(current->reqList.begin(), current->reqList.end(), request), current->reqList.end());
        if(current->reqList.empty()) {
            for(int i=0;i<=lvl;i++)
            {
                if(update[i]->forward[i] != current)
                    break;

                update[i]->forward[i] = current->forward[i];
            }
            while(lvl>0 &&header->forward[lvl] == 0)
                lvl--;

        }

    }

}
