#include "cell.h"

extern "С" {
    Cell* create_cell(char *name, int start, int request_type, bool request_flag) {
        return new Cell(name, start, (Types)request_type, request_flag);
    }

    bool check_flag(Cell *cell) {
        return cell->check_flag();
    }

    int get_end(Cell *cell) {
        return cell->get_end()
    }

    void del_cell(Cell *cell) {
        delete cell;
    }
}