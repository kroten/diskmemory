#include "model.h"
#include "area.h"
#include "cell.h"

int main() {
    SkipList lst(5, 0.8);
    std::multimap<int, Request *> desc_mv ={{2, new Area("mv", 1, 3, read)},
                                           {4, new Cell("mv", 20, read, false)},
                                           {10, new Area("mv", 5, 10, read)}
    };

    std::multimap<int, Request *> desc_cat={{5, new Area("cat", 1, 3, write)},
                                       {3, new Cell("cat", 5, write, true)}

    };

    std::multimap<int, Program *> mdesc = {{4, new Program (desc_mv, &lst)},
                                      {4, new Program (desc_cat, &lst)}
    };
    Model M(mdesc);
    M.StartModel();
    return 0;
}

