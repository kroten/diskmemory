import ctypes

disclib = ctypes.CDLL('./disclib.so')

disclib.create_skip_list.restype = ctypes.c_void_p
disclib.create_skip_list.argtypes = [ctypes.c_int, ctypes.c_float]

disclib.create_model.restype = ctypes.c_void_p
disclib.create_model.argtypes = [ctypes.c_void_p]

disclib.add_program.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int]

disclib.add_area.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.c_char_p, ctypes.c_int, ctypes.c_int, ctypes.c_int]

disclib.add_cell.argtypes = [ctypes.c_void_p, ctypes.c_int, ctypes.c_char_p, ctypes.c_int, ctypes.c_int, ctypes.c_bool]

disclib.del_program.argtypes = [ctypes.c_void_p, ctypes.c_char_p]

disclib.show_model.argtupes = [ctypes.c_void_p]

disclib.start_model.argtypes = [ctypes.c_void_p]

disclib.del_model.argtypes = [ctypes.c_void_p]

disclib.del_skip_list.argtypes = [ctypes.c_void_p]

print("input lvl and probability:")
lvl = int(input())
P = float(input())
lst = disclib.create_skip_list(lvl, P)
model = disclib.create_model(lst)

while 1:
    print("1 - add program in model\n2 - add area-request in program\n3 - add cell-request in program\n4 - delete program\n5 - show model\n6 - start model")
    i = int(input())
    if i == 1:
        print("input name and start")
        name = input()
        start = int(input())
        disclib.add_program(model, name.encode('utf-8'), start)
    if i == 2:
        print("input start time, name, start, end, type")
        start_time = int(input())
        name = input()
        start = int(input())
        end = int(input())
        type = int(input())
        disclib.add_area(model, start_time, name.encode('utf-8'), start, end, type)
    if i == 3:
        print("input start time, name, start, type, request flag")
        start_time = int(input())
        name = input()
        start = int(input())
        type = int(input())
        request_flag = bool(input())
        print(request_flag)
        disclib.add_cell(model, start_time, name.encode('utf-8'), start, type, request_flag)
    if i == 4:
        print("input name")
        name = input()
        disclib.del_program(model, name.encode('utf-8'))
    if i == 5:
        disclib.show_model(model)
    if i == 6:
        disclib.start_model(model)
        disclib.del_model(model)
        disclib.del_skip_list(lst)
        break


"""

libarea.create_area.restype = ctypes.c_void_p
libarea.create_area.argtypes = [ctypes.c_char_p, ctypes.c_int, ctypes.c_int, ctypes.c_int]

libarea.get_end.restype = ctypes.c_int
libarea.get_end.argtypes = [ctypes.c_void_p]

libarea.check_flag.restype = ctypes.c_bool
libarea.check_flag.argtypes = [ctypes.c_void_p]

libarea.change_type.restype = ctypes.c_bool
libarea.change_type.argtypes = [ctypes.c_void_p]

area = libarea.create_area('mv'.encode('utf-8'), 2, 5, 0)
print(libarea.get_end(area))
print(libarea.check_flag(area))
print(libarea.change_type(area))
"""
