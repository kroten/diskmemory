/*!
 * @file
 * Header file with description of the Program class
 */
#ifndef DISKMEMORY_PROGRAM_H
#define DISKMEMORY_PROGRAM_H


#include "skiplist.h"
#include <map>
#include <iostream>

/*!
 * @brief Program class
 *
 * This class is designed to simulate program that makes requests at the appointed time
 */
class Program {
private:
    std::string name; //!< program name
    std::multimap<int, Request *> description; //!< program description
    int lastStep = 0; //!< last request step
    NodeList* status = NULL; //!< pointer to the skip list node on which the hang occurred
    int frozen_step = 0; //!< The number of steps in standby mode
    int count_req = 0; //!< The number of requests that managed to be completed at the freeze step
    SkipList *lst; //!< pointer to skip list

public:
    /*!
     * create program
     * @param description pointer to description
     * @param lst pointer to skip list
     */
    Program(const std::multimap<int, Request*> &description, SkipList* lst);

    /*!
     * delete cell
     * @param it cell request iterator
     * @return description without cell request
     */
    std::multimap<int, Request*>::iterator deleteCell(std::multimap<int, Request*>::iterator it);

    /*!
     * do request
     * @param start program start time
     * @param step request time
     * @return 0 if request completed successfully, 1 if  hang occurred, 2 if program has finished work
     */
    int doRequest(int start, int step);

    NodeList* get_status();

    void set_name(const std::string &name) {
        this->name = name;
    }

    std::string get_name() {
        return this->name;
    }

    void add_request(int start, Request *req) {
        this->description.emplace(start, req);
        auto it = description.rbegin();
        this->lastStep = it->first;
    }

    void show_request() {
        if (this == NULL)
            return;
        for(auto it = this->description.begin(); it != this->description.end(); ++it) {
            std::cout << "\t Start: " << it->first << " Name: " << it->second->get_name() << " Start cell: " << it->second->get_start()
            << " End cell: " << it->second->get_end() << " Type: " << it->second->check_type()
            << " Flag: " << it->second->check_flag() <<'\n';
        }
    }
};


#endif //DISKMEMORY_PROGRAM_H
