//
// Created by kroten on 27.04.2020.
//

#include <iostream>
#include "model.h"

Model::Model(const std::multimap<int, Program *> &model) {
    this->model = model;

}

bool check_cycle(Program *start, Program *cur) {
    auto it = cur->get_status()->reqList.begin();
    while(it != cur->get_status()->reqList.end()) {
        auto next = reinterpret_cast<Program *>((*it)->get_program());
        if(next->get_status() == NULL) {
            ++it;
        }
        else {
            auto it_sub = (next->get_status()->reqList).begin();
            while(it_sub != (next->get_status()->reqList).end()) {
                auto nextnext = reinterpret_cast<Program *>((*it_sub)->get_program());
                if(nextnext == start)
                    return true;
                else
                    ++it_sub;
            }
            it_sub = (next->get_status()->reqList).begin();
            while(it_sub != (next->get_status()->reqList).end()) {
                auto nextnext = reinterpret_cast<Program *>((*it_sub)->get_program());
                bool t = check_cycle(start, nextnext);
                if(t)
                    return t;
                else
                    ++it_sub;
            }
            }
        }
    return false;
    }




void Model::StartModel() {
    for(int i = 0;i < 1000000; i++){
        this->time = i;
        for(auto it = this->model.begin(); it != this->model.end();) {
                int code = it->second->doRequest(it->first, i);
                if(code == 1) {
                    bool err = check_cycle(it->second, it->second);
                    if(err) {
                        printf("cycle");
                        return;
                    }
                }
                if(code == 2) {
                    this->model.erase(it);
                    if (this->model.empty()) {
                    printf("time: %d", this->time);
                    return;
                    }
                }
                ++it;
            }
        }
    printf("%d", this->time);
    }

void Model::add_program(int start, Program *prog) {
    this->model.emplace(start, prog);
}

void Model::add_request(const std::string &name, int time, Request *req) {
    for(auto it = this->model.begin(); it!= this->model.end(); ++it) {
        if(it->second->get_name() == name) {
            it->second->add_request(time, req);
            break;
        }
    }

}

void Model::del_program(const std::string &name) {
    for(auto it = this->model.begin(); it!= this->model.end(); ++it) {
        if(it->second->get_name() == name) {
            this->model.erase(it);
            break;
        }
    }
}

void Model::show_model() {
    if(this == NULL)
        return;
    for(auto it = this->model.begin(); it != this->model.end(); ++it) {
        std::cout << "Start: " << it->first << " Name: " << it->second->get_name() << '\n';
        it->second->show_request();
    }
}


