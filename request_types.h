/*!
 * @file
 * Header file with description of request types
 */
#ifndef DISKMEMORY_REQUEST_TYPES_H
#define DISKMEMORY_REQUEST_TYPES_H

//! set of possible request types
enum Types{
    read, //!< read request
    write //!< write request
};

#endif //DISKMEMORY_REQUEST_TYPES_H
