#include "model.h"
#include "area.h"
#include "cell.h"
#include <stdbool.h>

extern "C" {
    SkipList* create_skip_list(int Maxlvl, float P) {
        return new SkipList(Maxlvl, P);
    }

    Model* create_model(SkipList* lst) {
        std::multimap<int, Program*> mdesc;
        auto *m = new Model(mdesc);
        m->set_skiplist(lst);
        return m;
    }

    void add_program(Model* model, char *name, int start) {
        std::multimap<int, Request*> pdesc;
        auto lst = model->get_skiplist();
        auto p = new Program(pdesc, lst);
        p->set_name(name);
        model->add_program(start, p);
    }

    void add_area(Model *model, int time, char *name, int start, int end, int type) {
        auto a  = new Area(name, start, end, (Types)type);
        model->add_request(name, time, a);
    }

    void add_cell(Model *model, int time, char *name, int start, int type, bool request_flag) {
        auto c = new Cell(name, start, (Types)type, request_flag);
        model->add_request(name, time, c);
    }

    void del_program(Model *model, char* name) {
        model->del_program(name);
    }

    void show_model(Model *model) {
        model->show_model();
    }

    void start_model(Model *model) {
        model->StartModel();
    }

    void del_model(Model *model) {
        delete model;
    }

    void del_skip_list(SkipList* lst) {
        delete lst;
    }
}
/*
int main() {
    auto lst = create_skip_list(6, 0.6);
    auto model = create_model(lst);
    add_program(model, "mv", 2);
    add_program(model, "mv2", 5);
    add_area(model, 2, "mv", 4, 6, 0);
    add_area(model, 5, "mv2", 4, 6, 0);
    show_model(model);
    start_model(model);
    return 0;

}
 */