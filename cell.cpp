/*!
 * @file
 * cpp file with Cell class methods bodies
 */
#include "cell.h"
// constructor Cell class
Cell::Cell(const std::string &name, int start, Types request_type, bool request_flag): Request(name, start, request_type) {
    this->request_flag = request_flag;
}

// check request flag
bool Cell::check_flag() const {
    return this->request_flag;
}

int Cell::get_end() const {
    return this->get_start();
}

