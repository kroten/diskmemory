#include "program.h"
#include "area.h"
#include "cell.h"
#include "skiplist.h"

int main() {
    std::multimap<int, Request *> description ={{1, new Area("mv", 14, 16, read)},
                                           {1, new Cell("mv", 20, read, false)},
                                           {1, new Area("mv", 17, 23, read)}
                                           };
    SkipList lst(5, 0.8);
    Program mv(description, &lst);
    int start = 4;
    int a = 0;
    for(int i = start; i<15; i++){

        a = mv.doRequest(start, i);
    }
    return 0;
}

