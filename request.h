/*!
 * @file
 * Header file with description of the Request class
 */
#ifndef DISKMEMORY_REQUEST_H
#define DISKMEMORY_REQUEST_H

#include "request_types.h"
#include <string>
#include <vector>
/*!
 * @brief Request class
 *
 * * This class is designed to simulate requests
 */
class Request {
private:
    std::string name; //!< program name
    int start; //!< start cell of memory
    Types request_type; //!< request type read or write
    int *p_program;
    friend class Area;

public:
    /*!
     * create request
     * @param name program name
     * @param start start of area
     * @param request_type request type read or write
     */
    Request(const std::string &name, int start, Types request_type);

    /*!
     *
     * @return start cell of memory
     */
    int get_start() const;

    std::string get_name() const;

    void give_program(int *program);

    int* get_program();


    virtual int get_end() const = 0;

    virtual bool check_flag() const = 0;

    Types check_type();
    /*!
     *
     * @param other other request
     * @return
     */
    bool operator==(const Request& other);

    /*!
     *
     * @param other other other request
     * @return
     */
    bool operator<(const Request& other);

    virtual ~Request() {}
};


#endif //DISKMEMORY_REQUEST_H
