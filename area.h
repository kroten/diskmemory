/*!
 * @file
 * Header file with description of the Area class
 */
#ifndef DISKMEMORY_AREA_H
#define DISKMEMORY_AREA_H

#include "request.h"
/*!
 * @brief Memory area request class
 *
 * This class is designed to simulate requests for a memory area on a disk
 */
class Area: public Request {
private:
    int end; //!< end of area
public:
    /*!
     * create request
     * @param name program name
     * @param start start of area
     * @param end end of area
     * @param request_type request type read or write
     */
    Area(const std::string &name, int start, int end, Types request_type);

    /*!
     * Change the request type from read to write
     *
     * @return true if the request type has changes, else false
     */
    bool change_type();

    /*!
     *
     * @return return end
     */
    int get_end() const;

    bool check_flag() const;

};


#endif //DISKMEMORY_AREA_H