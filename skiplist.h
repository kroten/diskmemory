/*!
 * @file
 * Header file with description of the SkipList class
 */
#ifndef DISKMEMORY_SKIPLIST_H
#define DISKMEMORY_SKIPLIST_H
#include "request.h"
#include <vector>


/*!
 * @brief Node of skip list
 *
 * This class describes a skip list node
 */
class NodeList {
public:
    NodeList **forward;; //!< pointer to pointers to the next no
    std::vector<Request*> reqList; //!< pointer to the request
    /*!
     * create node
     * @param lvl node height
     * @param request request
     */
    NodeList(int lvl, Request *request);
};

/*!
 * @brief SkipList
 *
 * This class describes a skip list
 */
class SkipList {
private:
    NodeList *header; //!< pointer to header
    int MaxLvl; //!< maximum possible level
    float P; //!< probability
    int lvl;//!< level
public:
    /*!
     * create skip list
     * @param MaxLvl maximum possible level
     * @param P probability
     */
    SkipList(int MaxLvl, float P);

    /*!
     * determines the height of the node
     * @return level
     */
    int randomLevel();

    /*!
     * insert request in skip list
     * @param request
     * @return if the program gains access to the disk, it returns NULL,
     * else the returns the poiter to node because of which it was not possible to gain access.
     */
    NodeList* inseretRequest(Request *request);

    /*!
     * delete request from skip list
     * @param request
     */
    void deleteRequest(Request *request);

    /*!
     * display skip list
     */
    void showList();

};


#endif //DISKMEMORY_SKIPLIST_H
