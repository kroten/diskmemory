/*!
 * @file
 * Header file with description of the Cell class
 */
#ifndef DISKMEMORY_CELL_H
#define DISKMEMORY_CELL_H

#include "request.h"
/*!
 * @brief Memory cell request class
 *
 * This class is designed to simulate request for a memory cell on a disk
 */
class Cell: public Request {
private:
    bool request_flag; //!< request duration flag

public:
    /*!
     * create request
     * @param name program name
     * @param start start cell
     * @param request_type request type read or write
     * @param request_flag request duration flag (if set to false, the request is destroyed immediately after satisfaction)
     */


    Cell(const std::string &name, int start, Types request_type, bool request_flag);
    /*!
     * check request flag
     * @return  false if this->request_flag == false, else true
     */
    bool check_flag() const;

    /*!
     *
     * @return end == start
     */
    int get_end() const ;
};


#endif //DISKMEMORY_CELL_H
