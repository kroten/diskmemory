#ifndef DISKMEMORY_MODEL_H
#define DISKMEMORY_MODEL_H
#include <map>
#include "program.h"
class Model {
private:
    std::multimap<int, Program*> model;
    SkipList* lst;
    int time = 0;

public:
    Model(const std::multimap<int, Program*> &model);

    void StartModel();

    void set_skiplist(SkipList* lst) {
        this->lst = lst;
    }

    SkipList* get_skiplist() {
        return this->lst;
    }

    void add_program(int start, Program *prog);

    void add_request(const std::string &name, int time, Request *req);

    void del_program(const std::string &name);

    void show_model();
};


#endif //DISKMEMORY_MODEL_H
