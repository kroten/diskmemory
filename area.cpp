/*!
 * @file
 * cpp file with Area class methods bodies
 */
#include "area.h"
#include <iostream>
// constructor Area class
Area::Area(const std::string &name, int start, int end, Types request_type): Request(name, start, request_type) {
    this->end = end;
}

// change request type from read to write
bool Area::change_type() {
    if(request_type == read) {
        request_type = write;
        return true;
    }
    else {
        std::cerr << "Type of request don't change" << std::endl;
        return false;
    }
}

int Area::get_end() const {
    return this->end;
}

bool Area::check_flag() const {
    return true;
}

