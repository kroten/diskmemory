#include "area.h"
#include <stdbool.h>

extern "C" {
Area* create_area(char *name, int start, int end, int type) {
    return  new Area(name, start, end, (Types)type);
}

bool change_type(Area* area) {
    return area->change_type();
}

int get_end(Area* area) {
    return area->get_end();
}

bool check_flag(Area* area) {
    return area->check_flag();
}

void del_area(Area *area) {
    delete area;
}
}