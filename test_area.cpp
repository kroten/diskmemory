/*!
 * @file
 * cpp file with Area class tests
 */
#include <cassert>
#include "area.h"
#include "cell.h"


void test_Area(){
    // create a request for the program cat
    Area cat("cat", 2, 10, read);
    // change request type from read to write
    assert(cat.change_type());
    // create a request for the program cp1
    Area cp1("cp1", 3, 50, write);
    // change request type from read to write
    assert(!cp1.change_type());
    // compares the field start <
    assert(cat<cp1);
    // create a request for the program cp2
    Area cp2("cp2", 2, 50, write);
    // compares the field start ==
    assert(cat==cp2);
    // compares the field start !=
    assert(!(cat==cp1));
    Cell touch("touch", 3, write, true);
    assert(cp1==touch);
    assert(cat.get_end() == 10);
}

int main(){
    //test_Area();
    Area* p = new Area("cp2", 2, 50, write);
    delete p;
    p = NULL;
    delete p;

    int* buffer = (int*) malloc(sizeof(int*));
    buffer = buffer;
    free(buffer);
    //buffer = NULL;
    free(buffer);

    return 0;
}